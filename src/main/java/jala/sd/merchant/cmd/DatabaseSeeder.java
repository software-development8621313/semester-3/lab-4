package jala.sd.merchant.cmd;

import jala.sd.merchant.models.Product;
import jala.sd.merchant.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Component
public class DatabaseSeeder implements CommandLineRunner {
    private final ProductRepository productRepository;

    @Autowired
    public DatabaseSeeder(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        seedProducts();
    }

    private void seedProducts() {
        if (productRepository.count() == 0) {
            List<Product> products = Arrays.asList(
                    new Product(UUID.randomUUID(), "Laptop Gamer Acer", new BigDecimal("3500.89"), 248),
                    new Product(UUID.randomUUID(), "Smartphone Xiaomi Redmi Note 13", new BigDecimal("1290.90"), 196),
                    new Product(UUID.randomUUID(), "Smartphone Samsung S23 Ultra", new BigDecimal("5090.90"), 96),
                    new Product(UUID.randomUUID(), "Smartphone POCO X6 Pro", new BigDecimal("3290.90"), 16),
                    new Product(UUID.randomUUID(), "Laptop Vaio", new BigDecimal("4290.90"), 32),
                    new Product(UUID.randomUUID(), "Tablet Samsung Galaxy Tab A7", new BigDecimal("299.99"), 150),
                    new Product(UUID.randomUUID(), "Monitor LG UltraWide 29\"", new BigDecimal("249.99"), 50),
                    new Product(UUID.randomUUID(), "Mechanical Keyboard Redragon K556", new BigDecimal("79.99"), 300),
                    new Product(UUID.randomUUID(), "Headphone Bluetooth JBL T450BT", new BigDecimal("39.99"), 400)
            );

            productRepository.saveAll(products);

            System.out.println("Database seeded.");
        } else {
            System.out.println("Database have data already.");
        }
    }
}
