package jala.sd.merchant.http.controllers;

import jakarta.validation.Valid;
import jala.sd.merchant.http.requestDTO.ProductRequestDTO;
import jala.sd.merchant.http.responseDTO.ProductResponseDTO;
import jala.sd.merchant.http.responseDTO.SaleResponseDTO;
import jala.sd.merchant.models.Product;
import jala.sd.merchant.services.ProductService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/products")
public class ProductController {
    private final ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<ProductResponseDTO> getProductById(@PathVariable UUID id) {
        var product = productService.findById(id);
        var response = new ProductResponseDTO(product.getId(), product.getLabel(), product.getPrice(), product.getStock());
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @GetMapping
    public ResponseEntity<List<ProductResponseDTO>> getAllProduct() {
        var products = productService.findAll();
        var response = products.stream()
                .map(product ->
                    new ProductResponseDTO(product.getId(), product.getLabel(), product.getPrice(), product.getStock()
                )
        ).toList();

        return ResponseEntity.status(HttpStatus.OK).body(response);
    }
}
