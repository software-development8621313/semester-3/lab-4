package jala.sd.merchant.http.responseDTO;

import jala.sd.merchant.models.Product;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

public record SaleResponseDTO(UUID id, int quantity, LocalDateTime saleDate, List<Product> products) {}
