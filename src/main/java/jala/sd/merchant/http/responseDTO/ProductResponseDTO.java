package jala.sd.merchant.http.responseDTO;


import java.math.BigDecimal;
import java.util.UUID;

public record ProductResponseDTO(UUID id, String label, BigDecimal price, int stock) {
}
