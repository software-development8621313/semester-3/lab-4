package jala.sd.merchant.http.requestDTO;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import org.hibernate.validator.constraints.Length;

import java.math.BigDecimal;

public record ProductRequestDTO(
        @NotBlank
        @Length(min = 1, max = 50)
        String label,

        @NotNull
        @Min(0)
        BigDecimal price,

        @Min(1)
        @NotNull
        int stock
) {
}
