package jala.sd.merchant.models;

import jakarta.persistence.*;
import lombok.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "TB_SALES")
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
public class Sale {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;

    @Column(name = "NR_QUANTITY")
    private int quantity;

    @Column(name = "VL_TOTAL_PRICE")
    private BigDecimal totalPrice;

    @Column(name = "DT_SALE_DATE", columnDefinition = "TIMESTAMP")
    private LocalDateTime saleDate;

    @OneToMany
    @JoinColumn(name = "product_id", referencedColumnName = "id")
    private List<Product> products;

    public Sale(int quantity, List<Product> products, BigDecimal totalPrice) {
        this.quantity = quantity;
        this.saleDate = LocalDateTime.now();
        this.products = products;
        this.totalPrice = totalPrice;
    }
}
