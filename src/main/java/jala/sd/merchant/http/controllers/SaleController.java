package jala.sd.merchant.http.controllers;

import jala.sd.merchant.http.requestDTO.SaleRequestDTO;
import jala.sd.merchant.http.responseDTO.SaleResponseDTO;
import jala.sd.merchant.services.SaleService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/sales")
public class SaleController {
    private final SaleService saleService;

    public SaleController(SaleService saleService) {
        this.saleService = saleService;
    }

    @GetMapping
    public ResponseEntity<List<SaleResponseDTO>> getAllSales() {
        var sales = saleService.findAll();
        var response = sales.stream().map(sale -> new SaleResponseDTO(sale.getId(), sale.getQuantity(), sale.getSaleDate(), sale.getProduct())).toList();

        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @PostMapping
    public ResponseEntity<SaleResponseDTO> makeSale(@RequestBody SaleRequestDTO data) {
        var sale = saleService.save(data.productId(), data.quantity());
        var response = new SaleResponseDTO(sale.getId(), sale.getQuantity(), sale.getSaleDate(), sale.getProduct());
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }
}
