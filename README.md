# Merchant

Supondo que você foi contratado para desenvolver uma API REST para uma loja online que vende produtos eletrônicos. A API
deve permitir que os clientes acessem informações sobre os produtos disponíveis e realizem compras. A loja possui um
banco de dados com duas tabelas principais: Produtos e Vendas.

## Features

### Product

- Um produto só pode ser vendido se houver stock disponível.
- O preço do produto não pode ser negativo.
- Descontos progressivos com base na quantidade de produtos comprados. Por exemplo, ofereça um desconto de 5% para
  compras de mais de 10 unidades de um produto e um desconto de 10% para compras de mais de 20 unidades.

#### Data

- id_produto (chave primária)
- nome: Nome do produto.
- price: Preço unitário do produto.
- stock: Quantidade disponível em stock do produto.

### Sale

- A quantidade vendida em uma única transação não pode ser negativa ou zero.
- A data da venda deve ser registrada automaticamente como a data atual.

#### Data

- id_venda (chave primária)
- id_produto (chave estrangeira)
- quantidade: A quantidade de unidades vendidas.
- data: Data da venda.

## Endpoints

### Listar Produtos Disponíveis:

- **Endpoint**: /products
- **Method**: _GET_
- **Description**: Lista todos os produtos disponíveis para venda.

```json
[
  {
    "id": "<id>",
    "nome": "<nome>",
    "price": "<price>",
    "stock": "<stock>"
  },
  {
    "id": "<id>",
    "nome": "<nome>",
    "price": "<price>",
    "stock": "<stock>"
  }
]
``` 

### Detalhes de um Produto:

- **Endpoint**: /products/{id}
- **Method**: _GET_
- **Description**: Detalhes de um produto específico com base no seu ID.

#### Response Body

```json
{
  "id": "<id>",
  "nome": "<nome>",
  "price": "<price>",
  "stock": "<stock>"
}
```

### Make a Sale:

- **Endpoint**: /sales
- **Method**: _POST_
- **Description**: O cliente realiza uma venda. Após a venda ser concluída com sucesso, o stock do produto deve ser atualizado.

#### Request Body

```json
{
  "product_id": "<id>",
  "quantity": 34
}
```

### Sale History:

- **Endpoint**: /sales
- **Method**: _GET_
- **Description**: Histórico de todas as vendas realizadas.

#### Response Body

```json
[
  {
    "id": "<id>",
    "quantity": "<qtt>",
    "date": "<date>",
    "product": {
      "id": "<id>",
      "nome": "<nome>",
      "price": "<price>",
      "stock": "<stock>"
    }
  },
  {
    "id": "<id>",
    "quantity": "<qtt>",
    "date": "<date>",
    "product": {
      "id": "<id>",
      "nome": "<nome>",
      "price": "<price>",
      "stock": "<stock>"
    }
  }
]
```
