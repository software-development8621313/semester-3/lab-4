package jala.sd.merchant.services;

import jala.sd.merchant.models.Product;
import jala.sd.merchant.models.Sale;
import jala.sd.merchant.repositories.ProductRepository;
import jala.sd.merchant.repositories.SaleRepository;
import jala.sd.merchant.services.exceptions.InsufficientStockException;
import jala.sd.merchant.services.exceptions.ProductNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;

@Service
public class SaleService {
    private final SaleRepository saleRepository;
    private final ProductRepository productRepository;

    @Autowired
    public SaleService(SaleRepository saleRepository, ProductRepository productRepository) {
        this.saleRepository = saleRepository;
        this.productRepository = productRepository;
    }

    public List<Sale> findAll() {
        return saleRepository.findAll();
    }

    public Sale save(List<UUID> productsId, int quantity) {
        List<Product> productsToSale = new ArrayList<>();
        double totalPrice = 0;

        for (UUID productId : productsId) {
            Optional<Product> productOrNothing = productRepository.findById(productId);

            if (productOrNothing.isEmpty()) {
                throw new ProductNotFoundException("Product Not Found.");
            }

            Product product = productOrNothing.get();

            if (product.getStock() < quantity) {
                throw new InsufficientStockException("Not Stock Available.");
            }

            product.setStock(product.getStock() - quantity);

            totalPrice += 0;

            productsToSale.add(product);
        }

        Sale newSale = new Sale(productsToSale.size(), productsToSale, calculateDiscount(quantity, BigDecimal.valueOf(totalPrice)));

        return saleRepository.save(newSale);
    }

    BigDecimal calculateDiscount(int quantity, BigDecimal totalPrice) {
        int discount = 0;

        for(int i = 0; i < quantity; i++) {
            if(i % 10 == 0 && quantity <= 70) {
                discount += 5;
            }
        }

        var rate = discount / 100;

        return totalPrice.add(totalPrice.multiply(BigDecimal.valueOf(rate)));
    }
}
